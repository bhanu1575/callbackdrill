/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs')

function createJSONFile(fileName,deleteFile){

    let filePath = `./public/${fileName}.json`;
    
    fs.writeFile(filePath,'Hello',(error)=>{
        if(error) {
            throw new Error(error)
        } else {
            console.log(`${fileName} created successfully`);
            if(deleteFile){
                deleteFile();
            }
        }
    })
}
function deleteJSONFile(fileName,createNewFile){
  
    fs.unlink(`./public/${fileName}.json`,(error)=>{
        if(error){
            throw new Error(error);
        } else {
            console.log(`${fileName} deleted successfully....`);
            if(createNewFile){
                createNewFile();
            }
        }
    })
}

module.exports = {createJSONFile,deleteJSONFile}