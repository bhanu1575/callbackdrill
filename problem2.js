/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs')


function writeData (fileName, writingData,callback){
    fs.writeFile(`./public/${fileName}`,JSON.stringify(writingData),(err)=>{ 
        if(err){
            console.log(err);
        }
        console.log(`created the file ${fileName}`);
        fs.appendFile('./public/filenames.txt',fileName+',',(err)=>{
            if(err){
                console.log(err);
            }
            console.log('added to filenames..');
            if(callback){
                callback();
            }
        })
    })
}
function deleteAllFiles(filenames){
    let files = (filenames.slice(0,-1).split(','));
    for(fileName of files){
        fs.unlink(`./public/${fileName}`,(err)=>{
            if(err){
                console.log(err);
            }
        })
    }
    fs.writeFile('./public/filenames.txt','',(err)=>{
        if(err){
            console.log(err);
        }
    })

}
function getData(filename,callback){
    fs.readFile(`./public/${filename}`,{encoding : "utf-8"},(error,data)=>{
        if(error){
            console.log(error);
        }  
        console.log('data read');  
        if(callback){
            callback(data);
        }

    })
}

module.exports = {getData, writeData, deleteAllFiles}

