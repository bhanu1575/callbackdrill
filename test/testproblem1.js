/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const {createJSONFile,deleteJSONFile} = require('../problem1');

try {
    for(let fileCount = 1; fileCount <= 2; fileCount++){
        createJSONFile(`file-${fileCount}`,()=>{
            deleteJSONFile(`file-${fileCount}`);
        }); 
    }
} catch (error) {
    console.log(error);
}